# Commitment to anti-racism


![headerimage](../resources/img/antiracismheader.png)


## A Fair Share to All

From the beginning, Beacon Food Forest was founded on the principle of creating a fair share for all. We began with an open harvest policy, giving away the fruits of our labor because access to healthy food shouldn’t depend on your ability to pay or work for it and shouldn’t be limited by your race, ethnicity, or ZIP code. 

Our committees have always made decisions in a non-hierarchical way, empowering people of diverse backgrounds to lead and learn from each other in planning and maintaining the site. 

We see a lot of ethnic diversity at our work parties, among our P-Patch gardeners, and with our interns and community service hours volunteers. However, the majority of core volunteers who make decisions about the site continue to be white folks. 

We are coming to terms with how this has shaped our organizational culture and what the barriers are to people of color becoming more involved. Reflection on these dynamics is now a regular agenda item at all Beacon Food Forest committee meetings.


## Developing resources for anti-racist action

In 2018, we held a three-day anti-racism training for all board members, staff, and core volunteers. One of the outcomes of this training was a push to diversify the Food Forest Collective board. Now, a third of our board members are people of color. 

One of our greatest challenges is that we don’t yet have the financial capacity to create more stipended and salaried positions that would enable sustained involvement from people who cannot afford to volunteer. 

We are investing the funding we currently have into development strategies that will put the Food Forest Collective on more stable financial footing in future so that we can then invest more into hiring educators and leaders of color.


## Garden mentorship program

### Why did we start this program?
- We recognized the need for our food forest to be more culturally inclusive of the community 

- As you can see in the photo below, half of the food forest is established, but the other half has the potential to receive input from BIPOC community members to ensure we grow culturally relevant foods to meet their needs

#### A shift in mindset:

- Following the work of Leah Penniman, we understand the racism inherent in the term “permaculture”

- A critique of food forests is that people experiencing food insecurity aren’t typically involved in planning and creating them, but the people who are don’t need the food as much

- According to 2010 census, 79.6% of Beacon Hill residents are BIPOC yet our committees average 65% white and 35% non-white.


### About the program:

**Program Objective:** to mentor BIPOC and other underserved organizations in developing gardens at BFF

**What we will provide:** a BIPOC mentor to help you set up garden space, access to water and tools, woodchips and cardboard for sheet mulching, referrals to sources of free or low-cost plants/seeds, and compost

**What you will provide:** a person who can coordinate this program at your organization, people interested in learning how to garden in a food forest, some plants/seeds

**How to get started:** please email cherry@foodforest.ngo to set up a tour and see what spaces are available and fill out an agreement 

***Please let us know if any BIPOC organizations that you work for, or that you know of, are interested in setting up a garden at the food forest!***

### Program development timeline: 

**Aug 2, 2021:** our community outreach coordinator (COC) proposed this idea to the board of directors; it was approved

**Aug 26, 2021:** COC posted about the program on several online community groups

**Sept 9, 2021:** COC met with interested community partners to finalize agreement

**Sept 18, 2021:** we held our first work party group with a BIPOC community partner, the Asian Counseling and Referral Service (ACRS)

***Future: we hope to continue building partnerships with BIPOC and other underserved organizations to develop a community food forest to feed all***

### We want to feed the community:

We harvested only **4254, 2751, 2823, and 3097 lbs of food** in the years **2017, 2018, 2019, and 2020** respectively. The Garden of Eden Project did a study integrating many annual and perennial fruits/vegetables and found that an _average of 10,840 and 10,621 lbs respectively could be grown per acre._

## Partnerships with organizations led by people of color

We seek to partner with educational institutions in our ethnically diverse neighborhood as well as with other South Seattle food projects that are run by and/or primarily serve Black, Indigenous, and people of color. We regularly share and highlight the voices of Black farmers, educators, organizers, and Black-led organizations in our social media posts and newsletter.

Here are a few of the local projects we recommend you learn about:

- ![Percussion Farms and Preserves](https://www.facebook.com/PercussionFarms/)

- ![Yes Farm - Black Farmers Collective](https://www.blackfarmerscollective.com/yes-farm)

- ![Nurturing Roots](https://www.facebook.com/NurturingRoots206/)

- ![Got Green](https://www.facebook.com/GotGreenSeattle)

- ![Puget Sound Sage](https://www.facebook.com/pugetsoundsage/)

If you want to dive deeper into the intersections between racism and food insecurity, check out these resources:

- ![Black Urban Growers](https://www.blackurbangrowers.org/)

- ![National Black Food and Justice Alliance](https://www.blackfoodjustice.org/)

- ![The National Black Farmers Association](https://www.nationalblackfarmersassociation.org/)

- ![Cooperative Food Empowerment Directive](https://www.cofed.coop/)


![For more please visit our website's anti-racism page](https://beaconfoodforest.org/peoplecare/commitmenttoantiracism)
