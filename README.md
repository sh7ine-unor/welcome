# Welcome


Here is what you will need to complete in this Welcome project: 
1. [ ] Read [Commitment to Anti-Racism](./01 Commitment to Anti-Racism)
2. [ ] Read [our Code of Conduct](./02 Code of Conduct)
3. [ ] [Create a BitWarden account and save your passwords to a secure vault](./03 How to BitWarden)
4. [ ] [Go through the GitLab tutorials and setup 2fa](./04 How to GitLab)
5. [ ] Create a file in the [Pledge](./Pledge) directory committing to anti-racism and BFF Code of Conduct [following the example pledge](./Pledge/{example}MORTIMER_TURTLEBREATH_PLEDGES_ANTI_RACISM.md).


## Suggestions for a new user
Once you have completed the above steps and are accepted into our GitLab community, you may also want to join our Discord to keep up-to-date on changes in Beacon Food Forest projects. Ask comunity members which groups/channels would be right for you!

## Description
The "Welcome" project is for the first stage of getting working with Beacon Food Forest Community files

## Usage
By using Beacon Food Forest's GitLab you hereby commit to:

- ![Anti-Racism](01 Commitment to Anti-Racism/Commitment to Anti-Racism.md)
- ![Code of Conduct](02 Code of Conduct/Code of Conduct.md)
- [Earthcare](https://beaconfoodforest.org/earthcare)

## Support
Hop on Discord and ask for access to the arbiter channel

## Contributing
Please get involved! Look around, ask questions, be curious and respectfully helpful.

## Authors and acknowledgment
Beacon Food Forest, 1st Arbiter

## License
Open source licensing. Information in here is for the community development of Beacon Hill Food Forest use and discretion, but only to others when deliberately and directly shared from Beacon Food Community members. If said express permission from active BIPOC/ally community members do not allow for the sharing of any information in this project and subsequent projects & subgroups and all related or pertaining to said projects & subgroups, to be used, replicated, profited from, in any capacity or in any time, space, reality, or construct of thought-gymnastics by any party (individual or otherwise) other than the Beacon Food Forest BIPOC community.

## Project status
Constantly in motion.
