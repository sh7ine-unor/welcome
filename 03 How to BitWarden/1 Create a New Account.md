# BitWarden Create a New Account


- [ ] [Create a new user account by following](https://bitwarden.com/help/create-bitwarden-account/#next-steps)
- [ ] Be sure to respond to the verification email you receive once creating your account
- [ ] [Setup 2-factor authentication](https://bitwarden.com/help/setup-two-step-login/) (It is recommended that you use an authenticator app like Duo mobile or a yubikey-- using a phone number for 2fa is *not* advised)
