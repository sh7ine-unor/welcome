1. [Go through these steps](https://bitwarden.com/help/getting-started-webvault/)
2. [For real, setup 2-factor authentication if you have not done so, yet](https://bitwarden.com/help/setup-two-step-login/)
