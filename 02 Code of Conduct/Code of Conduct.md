# Code of conduct

![headerimage](../resources/img/codeconduct.png)

## We strive to actively create an inclusive and compassionate community

By joining any of our classes, gatherings or events, you agree to help maintain a safe and welcoming space for all, by adhering to the P-Patch Code of Conduct.

Discriminatory or threatening behaviors can be reported to the work party leader or class facilitator, or directly to our board at board@foodforest.ngo.

## Conflict Resolution Policy

Beacon Food Forest is committed to fostering equity, understanding and kindness throughout our community and the world. 

Conflict Resolution Norms:

- Assume positive intent.

- Own your impact.

- Express your feelings and needs with “I” statements only.

- Stay vulnerable.

- Focus on the issue and actions. Do not comment on each other’s identity.

- Speak impeccably -- mean what you say and say what you mean.

- Commit wholeheartedly to mutual reconciliation.

First, try to resolve the conflict between yourself and the other person. 

1. Communication: Can you resolve the issue by talking it out?

    1. First, take some time to reflect.

    2. What was your need in the situation? How could it have been addressed?

    3. Next, in turns, express your needs to each other. Share with each other how your needs could have been met.

2. Concession: How can you both meet each other’s needs?

    1. Concession is not compromise. How can the two of you work together to meet each other’s needs in the future?

    2. Choose a fair solution for both parties.

3. Consolidation

    1. Shake on your solution. (You may also perform some other form of salutation together that is culturally appropriate if handshaking is inappropriate for you.)

    2. Commit to holding each other accountable.

    3. Stay open and vulnerable enough with each other to accept each other’s feedback.


If you cannot resolve the issue yourselves, ask for a mediator. There is a designated Food Forest leader who can act as a mediator at every event. The mediator will walk you through the steps of reconciliation together. Your conflict and resolution steps will be documented. 

If you cannot resolve your issue with a Food Forest mediator, the issue will be escalated to Seattle P-patch leaders, who will walk you through further steps of reconciliation. 

If a Food Forest community member commits egregiously offensive acts, including, but not limited to hate speech, discriminatory behavior, or physical aggression or violence, a probationary period may be considered to protect the sacred and safe space of the Food Forest.
