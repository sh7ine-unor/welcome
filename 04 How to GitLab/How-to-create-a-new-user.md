# How to Make a New GitLab account

1. Go to https://gitlab.com

2. Click on the user icon to create a new user or sign in

![create new user]()

3. Register for a new account

![register]()

4. Fill in the following parts:
    1. Name (doesn't have to be full name)
    2. username (please make it SFW)
    3. The email address you will use for signing up and receiving notifications from gitlab
    4. A strong password (go to BitWarden for creating a password vault)
    5. Prove you are not a robot
    6. Click "register"

![register info]()

5.
