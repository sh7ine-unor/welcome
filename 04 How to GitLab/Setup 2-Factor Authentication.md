# 2-Factor Authentication (2FA)


## What is it?

Every online account worth its salt uses a password for authentication. In the digital age, one layer of security is no longer the best practice with [phishing scams](https://nakedsecurity.sophos.com/2014/12/29/yes-i-got-an-itunes-gift-card-for-christmas-but-how-did-the-crooks-know-that/), [social hacking](https://resources.infosecinstitute.com/topic/social-engineering-a-hacking-story/), and [brute-force attacks](https://www.fortinet.com/resources/cyberglossary/brute-force-attack).

That is why IT security has been using 2-factor authentication for a while now, as have financial institutions. One extra layer of security using a device that only the true owner of the account would have-- a phone with an app giving a secondary auth code when you use your password, or a physical security key you must insert into the device you are logging on with. 

For our GitLab projects, we enforce 2fa to ensure all our users have security on their accounts. If you have questions about setting it up, please reach out on Discord to the `#arbiter` channel.


## How to Setup 2FA on GitLab

[Follow these steps on setting up 2fa](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#enable-two-factor-authentication). I recommend getting an app on your phone like [Duo Mobile](https://duo.com/product/multi-factor-authentication-mfa/duo-mobile-app) or getting yourself a [security key like Yubico](https://www.yubico.com/products/).

This is not exclusive to GitLab-- you can setup 2fa on your gmail, Discord and other accounts. Feel fre to setup on BitWarden as well for extra security for your saved passwords.
