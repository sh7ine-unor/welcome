# Making a Pledge File

In order to make a pledge to anti-racism and our Code of Conduct, [please go through this tutorial on how to request a file creation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-add-edit-or-upload-a-file).


Save this file within the [Pledge](./Pledge) directory.

When you make the file, be sure to sign the file digitally by naming it "<YOUR_NAME>_PLEDGES_ANTI_RACISM.md" with your name as a signature at the bottom of the file. Below is an example:


```
I pledge to honor Permaculture's ethics of Earth Care, People Care, and Fair share. I commit to a non-hierarchical, racially diverse, and to educate myself, as a non-BIPOC individual, about the generational effects of white supremacy and how to create holistic community in the face of oppression. I commit to acting on anti-racist principles and to enact anti-racism in my endeavors with Beacon Food Forest and outside of it. I commit to becoming a better ally to BIPOC, and to honor their spaces & sovereignty, and to keep my mouth shut when I do not know what I am talking about. It is nobody else's job to educate me but my own, and I will give effort toward dismantling what racist systems and generationally engrained mannerisms, habits, and beliefs are present within myself and my community.

I commit to the Beacon Food Forest Code of Conduct by pledging to:

- Assume positive intent.

- Own my impact.

- Express your feelings and needs with “I” statements only.

- Stay vulnerable.

- Focus on the issue and actions. Do not comment on each other’s identity.

- Speak impeccably -- mean what you say and say what you mean.

- Commit wholeheartedly to mutual reconciliation.


These words and commitments are my own and I stand by them now and always,
Mortimer Turtlebreath
```
